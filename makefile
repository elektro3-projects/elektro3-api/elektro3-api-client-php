MAKEFLAGS += --silent

PROJECT_NAME = elektro3-api-old-examples

DOCKER_NGINX = elektro3-api-old-examples-php-nginx
DOCKER_PHP = elektro3-api-old-examples-php-php

Color_Off=\033[0m
BYellow=\033[1;33m
BIYellow=\033[1;93m
Green=\033[0;32m
UCyan=\033[4;36m

help: ## Show this help message
	echo 'Elektro API testing docker environment'
	echo 'usage: make [command]'
	echo
	echo 'commands:'
	egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'

up: ## Starts the app, creates the containers if necessary
	docker-compose -p ${PROJECT_NAME} --file docker/docker-compose.yml up -d
	## docker exec -it -u root ${DOCKER_NGINX} composer update -d /var/www
	printf "··············································································\n";
	printf "${BIYellow}Elektro3${Color_Off} API testing environment\n"
	printf "Running at: ${UCyan}http://localhost:8080${Color_Off}\n"
	printf "Remember to add your Elektro3 API credentials to the ${Green}app/config.php${Color_Off} file\n"
	printf "··············································································\n";

down: ## Stops the app, removes the containers
	docker-compose -p ${PROJECT_NAME} --file docker/docker-compose.yml down

start: ## Starts the app
	docker-compose -p ${PROJECT_NAME} --file docker/docker-compose.yml start

stop: ## Stops the app
	docker-compose -p ${PROJECT_NAME} --file docker/docker-compose.yml stop

restart: ## Restarts the app
	$(MAKE) stop && $(MAKE) start

status: ## Information about the containers
	docker-compose -p ${PROJECT_NAME} --file docker/docker-compose.yml ps

build: ## Builds the images
	docker-compose -p ${PROJECT_NAME} --file docker/docker-compose.yml build nginx
	docker-compose -p ${PROJECT_NAME} --file docker/docker-compose.yml build php

ssh: ## SSH into the php container
	docker exec -it -u root ${DOCKER_PHP} bash

log: ## Tail the PHP error log
	docker logs -f --details ${DOCKER_PHP}
