<?php

/**
 * Elektro3 API client PHP class
 * Provides easy access to the Elektro3 API.
 * @version 1.1.0
 *
 * Check out the official repository for this class here:
 * https://gitlab.com/elektro3-projects/elektro3-api/elektro3-api-old-examples-php
 * to find the latest version, examples and a testing environment.
 *
 * The [Elektro3 API documentation](https://elektro3-projects.gitlab.io/elektro3-api/elektro3-api-old-doc/es)
 * provides thorough information, examples and specifications for all available
 * endpoints, filtering options and advanced techniques.
 *
 * # Changelog
 * - v1.1 Provides the new `query` method, whichs allows for easier requests to
 *   the API by automatically taking care of requesting the access token when
 *   needed.
 * - v1.0 First version
 *
 * # License
 * This software is licensed exclusively to Elektro3 clients as long as they
 * retain their client condition, cannot be modified nor distributed, and can
 * only be used for the purpose of accessing the Elektro3 API.
 *
 * This software is provided as-is, without any warranty, and should be used
 * only by accredited professionals. Elektro3 is not held liable for any
 * damages caused by the misuse, faulty implementation or any other use of this
 * software.
 */

namespace Elektro3;

const PERSISTENT_STORAGE_APC = 0;
const PERSISTENT_STORAGE_APCU = 1;

const PERSISTENT_STORAGE_ACCESS_TOKEN_TTL = 3600;

class ApiClient {
	private $baseEndpoint = 'https://api.elektro3.com';
	private $clientId;
	private $clientSecret;
	private $username;
	private $password;
	private $accessToken = null;

	function __construct($setup) {
		$this->clientId = $setup['clientId'];
		$this->clientSecret = $setup['clientSecret'];
		$this->username = $setup['username'];
		$this->password = $setup['password'];
		$this->retrievePersistentlyStoredAccessToken();
	}

	/**
	 * Gets the best persistent storage mechanism name available, if there's any
	 * @return int|bool One of the PERSISTEN_STORAGE_* constants, if there is any available, or false if there is none.
	 */
	function getPersistentStorageMethod() {
		if (function_exists('apc_fetch'))
			return PERSISTENT_STORAGE_APC;
		if (function_exists('apcu_fetch'))
			return PERSISTENT_STORAGE_APCU;
		return false;
	}

	/**
	 * @return bool Whether there is any persistent storage mechanism available
	 */
	function isPersistentStorage() {
		return $this->getPersistentStorageMethod() ? true : false;
	}

	/**
	 * Retrieves the name of the specified persistent storage method
	 * @param int $persistentStorageMethod One of the PERSISTEN_STORAGE_* constants
	 * @return string The name of the persistent storage method
	 */
	function getPersistentStorageMethodName($persistentStorageMethod) {
		return [
			PERSISTENT_STORAGE_APC => 'APC',
			PERSISTENT_STORAGE_APCU => 'APCu'
		][$persistentStorageMethod];
	}

	/**
	 * Retrieves the acces token from any available local storage mechanism if it was persistently stored before
	 */
	function retrievePersistentlyStoredAccessToken() {
		if (!$this->isPersistentStorage())
			return;

		switch ($this->getPersistentStorageMethod()) {
			case PERSISTENT_STORAGE_APC:
				if ($accessToken = apc_fetch('elektro3ApiAccesstoken'))
					$this->accessToken = $accessToken;
				break;
			case PERSISTENT_STORAGE_APCU:
				if ($accessToken = apcu_fetch('elektro3ApiAccesstoken'))
					$this->accessToken = $accessToken;
				break;
		}
	}

	/**
	 * Persistently stores the given access token if there's any persistent storage available
	 * @param string $accessToken The access token
	 */
	function storePersistentAccessToken($accessToken) {
		if (!$this->isPersistentStorage())
			return;

		switch ($this->getPersistentStorageMethod()) {
			case PERSISTENT_STORAGE_APC:
				apc_store('elektro3ApiAccesstoken', $accessToken, PERSISTENT_STORAGE_ACCESS_TOKEN_TTL);
				break;
			case PERSISTENT_STORAGE_APCU:
				apcu_store('elektro3ApiAccesstoken', $accessToken, PERSISTENT_STORAGE_ACCESS_TOKEN_TTL);
				break;
		}
	}

	/**
	 * Queries the API, automatically retrieving an access token when needed.
	 * This method provides an easy way of querying the API, as takes care automatically of requesting the access token when needed.
	 * @param string $endpoint The endpoint. E.g. `/api/get-productos`
	 * @param array $post The parameters to add to the API request, as a hash array where each key is the name of the parameter, and each value is the desired parameter's value
	 * @param array $options An optional array of additional parameters to pass to cURL when performing the request.
	 * @throws ApiException If there was an error while performing any of the required requests.
	 * @return object|null The results of the request as provided by the API.
	 */
	public function query(
		$endpoint,
		$post = [],
		$options = []
	) {
		if (is_null($this->accessToken)) {
			$this->accessToken = $this->getAccessToken();
			$this->storePersistentAccessToken($this->accessToken);
		}
		return $this->request(
			$this->accessToken,
			$endpoint,
			$post,
			$options
		);
	}

	/**
	 * Performs a request to the API.
	 * @param string $accessToken The access token. E.g. `eyJ0eXAiOiJKV1QiLCJh[...]`
	 * @param string $endpoint The endpoint. E.g. `/api/get-productos`
	 * @param array $post The parameters to add to the API request, as a hash array where each key is the name of the parameter, and each value is the desired parameter's value
	 * @param array $options An optional array of additional parameters to pass to cURL when performing the request.
	 * @throws ApiException If there was an error while performing the request.
	 * @return object|null The results of the request as provided by the API.
	 */
	public function request(
		$accessToken,
		$endpoint,
		$post = [],
		$options = []
	) {
		$defaults = array(
			CURLOPT_POST => 1,
			CURLOPT_HEADER => 0,
			CURLOPT_URL => $this->baseEndpoint.$endpoint,
			CURLOPT_FRESH_CONNECT => 1,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_FORBID_REUSE => 1,
			CURLOPT_TIMEOUT => 300,
			CURLOPT_POSTFIELDS => http_build_query($post)
		);

		$options[CURLOPT_HTTPHEADER][] = 'Accept: application/json';

		if ($accessToken)
			$options[CURLOPT_HTTPHEADER][] = 'Authorization: Bearer '.$accessToken;

		$ch = curl_init();
		curl_setopt_array($ch, $options + $defaults);
		if (!$result = curl_exec($ch))
			throw new ApiException(curl_error($ch));
		curl_close($ch);
		if (is_null($r = json_decode($result)))
			throw new ApiException('Could not parse API answer, JSON error: '.json_last_error_msg());
		return $r;
	}


	/**
	 * Performs a request to the API to obtain an access token
	 * @throws ApiException If there was an error while performing the request to obtain the access token.
	 * @return string The API access token.
	 */
	public function getAccessToken() {
		$result = $this->request(
			false,
			'/oauth/token',
			[
				'client_id' => $this->clientId,
				'grant_type' => 'password',
				'client_secret' => $this->clientSecret,
				'username' => $this->username,
				'password' => $this->password
			]
		);
		if (isset($result->error))
			throw new ApiException('Exception when requesting access token: '.$result->message);
		return $result->access_token;
	}
}

class ApiException extends \Exception {
}
