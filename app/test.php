<?php

// A script that tests the environment to check that queries to the API can be done correctly.

header('Content-type: text/plain; charset=utf-8');

test(
	'Required files',
	function() {
		return includeFiles([
			['config.php', 'Copy the provided "config.php.example" file to create the file "config.php" and edit it with your API credentials and other specific configuration settings.'],
			['ApiClient.php', 'It looks the repository wasn\'t cloned correctly, or you copied some files while leaving others. Because of that, this examples kit would not be able to work.'],
		]);
	},
	'All required files present'
);

test(
	'PHP version',
	function() {
		if (version_compare(phpversion(), '5.6.0', '<'))
			return [false, 'PHP Version 5.6.0 or higher is required'];
		else
			return [true];
	},
	'PHP version '.phpversion().' version ok'
);

test(
	'PHP environment',
	function() {
		return checkFunction([
			'curl_init',
			'curl_setopt_array',
			'curl_close',
			'curl_error',
			'json_decode'
		]);
	},
	'All critical required functions present'
);

test(
	'API credentials configuration',
	function() {
		$isError = false;
		$descriptions = [];
		foreach ([
			'API_CLIENT_ID',
			'API_CLIENT_SECRET',
			'API_USERNAME',
			'API_PASSWORD'
		] as $define) {
			if (!defined($define) || empty(constant($define))) {
				$isError = true;
				$descriptions[] = 'API configuration "'.$define.'" has not been set in "config.php"';
			}
		}

		if (!ctype_digit(API_CLIENT_ID)) {
			$isError = true;
			$descriptions[] = 'Wrong "API_CLIENT_ID" in "config.php", must be a number';
		}

		return [
			!$isError,
			$descriptions
		];
	},
	'All API credentials present and ok'
);

test(
	'API connection',
	function() {
		return checkConnection('https://api.elektro3.com');
	},
	'Connection established'
);


$api = null;
$token = null;
test(
	'API token retrieval',
	function() {
		global $api;
		global $token;
		try {
			$api = new Elektro3\ApiClient([
				'clientId' => API_CLIENT_ID,
				'clientSecret' => API_CLIENT_SECRET,
				'username' => API_USERNAME,
				'password' => API_PASSWORD
			]);
			$token = $api->getAccessToken();
		}
		catch (Exception $e) {
			return [false, 'Could not retrieve token: '.$e->getMessage()];
		}
		return [true, 'Token retrieved: '.substr($token, 0, 20).'[...]'];
	}
);

$categories = null;
test(
	'API categories retrieval',
	function() {
		global $api;
		global $token;
		$productCode = '85807';

		try {
			$result = $api->request(
				$token,
				'/api/get-categorias',
				[
					'iso_code' => 'es'
				]
			);
		}
		catch (Exception $e) {
			return [false, 'Could not retrieve categories: '.$e->getMessage()];
		}

		if ($result->status == 0)
			return [false, 'Error requesting the API for categories'];
		else
		if ($result->status == 2)
			return [false, 'Missing parameters when retrieving categories'];

		global $categories;
		$categories = $result->categorias;
		return [true, count($result->categorias).' categories retrieved'];
	}
);

$categoryProducts = null;
test(
	'API products retrieval',
	function() {
		global $api;
		global $token;
		global $categories;
		global $categoryProducts;

		$count = 0;
		while (true) {
			$randomCategory = $categories[rand(0, count($categories) - 1)];

			try {
				$result = $api->request(
					$token,
					'/api/get-productos-categoria',
					[
						'category_code' => $randomCategory->codigo,
						'iso_code' => 'es'
					]
				);
			}
			catch (Exception $e) {
				return [false, 'Could not retrieve products in category '.$randomCategory->codigo.': '.$e->getMessage()];
			}

			if ($result->status == 0)
				return [false, 'Error requesting the API for products in category '.$randomCategory->codigo];
			else
			if ($result->status == 2)
				return [false, 'Missing parameters when retrieving products in category '.$randomCategory->codigo];

			if (++$count === 30)
				return [false, 'Too many attempts at finding a category with at least one product'];

			if (count($result->productos) === 0)
				continue;

			$categoryProducts = $result->productos;
			return [true, count($result->productos).' products in category "'.$randomCategory->nombre.'" retrieved ok'];
		}
	}
);

test(
	'API product retrieval',
	function() {
		global $api;
		global $token;
		global $categoryProducts;

		$randomProduct = $categoryProducts[rand(0, count($categoryProducts) - 1)];

		try {
			$result = $api->request(
				$token,
				'/api/get-producto',
				[
					'product_code' => $randomProduct->codigo,
					'iso_code' => 'es'
				]
			);
		}
		catch (Exception $e) {
			return [false, 'Could not retrieve product code '.$randomProduct->codigo.': '.$e->getMessage()];
		}

		if ($result->status == 0)
			return [false, 'Error requesting the API for product code '.$randomProduct->codigo];
		else
		if ($result->status == 2)
			return [false, 'Missing parameters when retrieving product code '.$randomProduct->codigo];

		return [true, 'Product code '.$randomProduct->codigo.' retrieved ok: "'.substr($result->productos[0]->nombre, 0, 30).'[...]"'];
	}
);

test(
	'Persistent storage',
	function() {
		global $api;
		$persistentStorageMethod = $api->getPersistentStorageMethod();
		if ($persistentStorageMethod === false)
			return [true, 'No persistent storage mechanism available, using local class caching'];
		return [true, 'Persistent storage "'.$api->getPersistentStorageMethodName($persistentStorageMethod).'" available'];
	}
);

test(
	'API query method',
	function() {
		global $api;
		global $categoryProducts;

		$randomProduct = $categoryProducts[rand(0, count($categoryProducts) - 1)];

		try {
			$result = $api->query(
				'/api/get-producto',
				[
					'product_code' => $randomProduct->codigo,
					'iso_code' => 'es'
				]
			);
		}
		catch (Exception $e) {
			return [false, 'Could not retrieve product code '.$randomProduct->codigo.' using query method: '.$e->getMessage()];
		}

		if ($result->status == 0)
			return [false, 'Error requesting the API using query method for product code '.$randomProduct->codigo];
		else
		if ($result->status == 2)
			return [false, 'Missing parameters when retrieving product code '.$randomProduct->codigo].' with query method';

		return [true, 'Product code '.$randomProduct->codigo.' retrieved ok using query method: "'.substr($result->productos[0]->nombre, 0, 30).'[...]"'];
	}
);

message('✅ All tests passed ok, you should be able to work with the Elektro3 API in this environment without any problems.');
message('ℹ️ Copy the files on this directory to a folder in your server and run index.php to test the Elektro3 API there.');

function test(
	$title,
	$function,
	$okMessage = false
) {
	$r = $function();
	$result = $r[0];
	$descriptions = isset($r[1]) ? $r[1] : null;
	if (!$result) {
		message('⚠️ Testing '.$title.' ... Failed'.($descriptions ? ':'."\n".implode("\n", (is_array($descriptions) ? $descriptions : [$descriptions])) : null));
		exit;
	}
	else
		message('☑️ Testing '.$title.' ... Passed'.($descriptions ? ': '.implode("\n", (is_array($descriptions) ? $descriptions : [$descriptions])) : null).($okMessage ? ': '.$okMessage : null));
}

function checkFunction($functionNames) {
	$isError = false;
	$descriptions = [];
	foreach ((!is_array($functionNames) ? [$functionNames] : $functionNames) as $functionName) {
		if (!function_exists($functionName)) {
			$isError = true;
			$descriptions[] = 'This PHP installation does not supports the required "'.$functionName.'" function. Enable the appropriate PHP extension or upgrade your PHP installation.';
		}
	}
	return [
		!$isError,
		$descriptions
	];
}

function includeFiles($files) {
	$isError = false;
	$descriptions = [];
	foreach ($files as $file) {
		if (!file_exists($file[0])) {
			$isError = true;
			$descriptions[] = 'File "'.$file[0].'" is required. '.$file[1];
		}
		else
			include $file[0];
	}
	return [
		!$isError,
		$descriptions
	];
}

function message($string) {
	echo $string."\n";
	flush();
	ob_flush();
}

function error($string) {
	message($string);
	die;
}

function checkConnection($hostName) {
	$options = array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_HEADER => 0,
		CURLOPT_URL => $hostName,
		CURLOPT_CONNECTTIMEOUT => 10,
		CURLOPT_HEADER => true,
		CURLOPT_NOBODY => true
	);

	$ch = curl_init();
	curl_setopt_array($ch, $options);
    if (!curl_exec($ch)) {
		$curlError = curl_error($ch);
		curl_close($ch);
		return [false, 'Connection error, could not connect to '.$hostName.' with error "'.$curlError.'"'];
	}

	if (!curl_errno($ch)) {
		$info = curl_getinfo($ch);
		curl_close($ch);
		return [true, 'Connection ok, '.number_format($info['total_time'] * 1000, 0, '.', '').' milliseconds response time'];
	}

	curl_close($ch);
	return [false, curl_error($ch)];
}
