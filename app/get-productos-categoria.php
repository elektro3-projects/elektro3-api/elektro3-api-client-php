<?php

// An example on how to use the Elektro3 API to get all the products in a category

header('Content-type: text/plain; charset=utf-8');
include 'config.php';
include 'ApiClient.php';

$api = new Elektro3\ApiClient([
	'clientId' => API_CLIENT_ID,
	'clientSecret' => API_CLIENT_SECRET,
	'username' => API_USERNAME,
	'password' => API_PASSWORD
]);

try {

	// Perform the desired API request
	$result = $api->query(
		'/api/get-productos-categoria',
		[
			'category_code' => '003303',
			'iso_code' => 'es',
			'page' => 1
		]
	);

	// Check for errors in the request
	if ($result->status == 0)
		throw new Exception('Error requesting the API');
	else
	if ($result->status == 2)
		throw new Exception('Missing parameters');

}
catch (Exception $e) {
	echo $e->getMessage()."\n";
	die;
}

echo count($result->productos)." total products found in the specified category, here are 3 random ones:\n";

$count = 1;
while ($count <= 3) {
	$product = $result->productos[rand(0, count($result->productos) - 1)];
	echo str_repeat('-', 80)."\n";
	print_r($product);
	$count ++;
}
