# Elektro API PHP Library
A PHP library to use the Elektro3 API, along with a testing environment and examples.
Examples on how to use the Elektro3 API, a test to determine compatibility and to help solve issues, and a class to interface easily with the Elektro3 API.

## Installation via composer
If you use `composer` in your project, install the library by running the following command:

```bash
composer require elektro3/api-client
```

## Manual installation
You can install the library without `composer` by copying the file `ApiClient.php` to your project and including it manually in your code like this:

```php
include 'ApiClient.php';
```

## Official API documentation
The [Elektro3 API documentation](https://elektro3-projects.gitlab.io/elektro3-api/elektro3-api-old-doc/es) provides thorough information, examples and specifications for all available endpoints, filtering options and advanced techniques.

> In the Elektro3 API documentation you'll also find explained examples on how to use this library.

## Example files
The `/app` directory on this repository contains several examples on how to use this library to query the Elektro3 API.

> You can bring up the testing environment to test the provided examples in your computer.

## Bringing up the testing environment
This repository additionally provides a docker-based testing environment that will allow you to test your credentials and run the Elektro3 API PHP library examples. Follow this steps to set up the environment:

- Under `/app`, copy the provided `config.php.example` to a new file called `config.php`, and edit it with your Elektro3 client credentials.
- Run `make up` to bring up the testing environment.
- Run the provided examples by pointing your browser to the appropriate URL in your PHP environment. For example: `http://localhost:8080/get-producto.php`
- Run `make log` to see any PHP errors.

## Testing your environment
A testing tool is provided to test the API connection in your server for compatibility. To run it:

- Copy the contents of the `/app` directory to your server.
- Copy the provided `config.php.example` to a new file called `config.php`, and edit it with your Elektro3 client credentials.
- Point your browser to the appropriate URL in your server matching the directory where you've uploaded the `/app` contents. For example: `http://<your server IP or domain>/elektro3-api-test`

You'll get a detailed report on your environment's compatibility and details about any issues found that will help you get your API connection up and running.

> You can also run the test in the local docker environment. To do so, bring up the testing environment by following the steps mentioned above and browse to `http://localhost:8080/test.php`

> You can of course use any other method to call the Elektro3 API, just like with any other REST-style API. If that's your case, you'll find all you need in the [official API documentation](https://elektro3-projects.gitlab.io/elektro3-api/elektro3-api-old-doc/es).
