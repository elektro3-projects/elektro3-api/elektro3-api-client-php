# Changelog
Changelog documentation for Elektro3's API examples. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2022-3-18
### Added
- The new `query` method, which allows for easier requests to the API by automatically taking care of requesting the access token when needed, using an automatically detected persistent storage method that supports APC, APCu and local object-level storage.
- Improved documentation.
- Added license information.
- Improved docker testing environment.

## [1.0.0]
First version
